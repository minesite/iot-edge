mkdir -p /artifacts/build/core && \
mkdir -p /artifacts/core && \
mkdir -p /artifacts/install-deps/lib && \
mkdir -p /artifacts/modules && \
cp -Rpv v1/build/core/libgateway.so /artifacts && \
cp -Rpv v1/build/bindings/java/libjava_module_host.so /artifacts && \
cp -Rpv v1/install-deps/lib/libaziotsharedutil.so /artifacts && \
cp -Rpv v1/install-deps/lib/libnanomsg.so /artifacts && \
cp -Rpv v1/install-deps/lib/libnanomsg.so.5.0.0 /artifacts && \
cp -Rpv v1/build/modules/iothub/libiothub.so /artifacts && \
cp -Rpv v1/core/inc /artifacts/core/inc && \
cp -Rpv v1/install-deps/include /artifacts/install-deps/include && \
cp -Rpv v1/modules/common /artifacts/modules/common

