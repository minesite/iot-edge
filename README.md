# MST fork of Microsoft IoT Edge V1

```bash
git clone --recurse-submodules -j8 https://github.com/Azure/iot-edge
```

## Ubuntu 16.04

### Build Docker container

```bash
./build.sh
```

### Run build

```bash
./run.sh
```

Artifacts copied to ./artifacts

## iOS

```bash
git clone https://bitbucket.org/minesite/ios-build-scripts
./ios-build-scripts/iot-edge/build_libaziotsharedutil.sh
./ios-build-scripts/iot-edge/build_libgateway.sh
```

## Experimental

### alpine

```bash
docker build -t build_iote_alpine:latest -f docker/Dockerfile_alpine .
docker run -v $(pwd)/iot-edge:/iot-edge -v $(pwd)/artifacts:/artifacts \
           -t build_iote_alpine:latest
```

### qemu_armv7

```bash
cp support/qemu_armv7/toolchain-armhf.cmake iot-edge/v1
docker build -t build_iote_qemu_armv7:latest -f docker/Dockerfile_qemu_armv7 .
docker run -v $(pwd)/iot-edge:/iot-edge -v $(pwd)/artifacts:/artifacts \
           -t build_iote_qemu_armv7:latest
```
