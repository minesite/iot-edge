# poky-linux hard-float arm7-a toolchain for mst-edge
# 
# 1) source /opt/fsl-imx-x11/4.1.15-2.0.0/environment-setup-cortexa9hf-neon-poky-linux-gnueabi
# 2). ./tools/build.sh --use-xplat-uuid --enable-java-binding --toolchain-file ./toolchainArm7a-poky-linux.cmake --disable-ble-module -cl -Wno-discarded-qualifiers -cl -Wno-deprecated-declarations -x
INCLUDE(CMakeForceCompiler)

SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_PROCESSOR armhf)

